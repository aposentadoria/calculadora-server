# calculadora-server

Este é um serviço de cálculo de aposentadoria, feito com objetivo de comparação de dois sitemas de aposentadoria em debate no Brasil em 2019:
* Modelo Presente (solidário)
* Modelo Proposto (capitalização)

Trata-se de uma estimativa com base em modelos de aproximação - portanto, embora o cálculo procure ser fiel às propostas, trata-se de uma aproximação de ambos os modelos, dado que há fatores que são desprezados pela metodologia.

Busca-se, com este serviço, oferecer insumos para que trabalhadores compreendam o funcionamento do sistema de aposentadoria e possam escolher qual modelo julgam mais adequado para o país.

Não cabe, aqui, entrar em uma análise crítica sobre os dois sistemas. Apenas expô-los de uma maneira mais didática para as pessoas.

# Modelo de Cálculo Proposto

Ao final deste documento, há um [descritivo](#descritivo-dos-modelos-de-aposentadoria) que explica melhor os fundamentos do modelo de cálculo proposto para os dois modelos de aposentadoria existentes.

## Premissas

Por tratar-se de um cálculo de aposentadoria simplificado, algumas premissas serão assumidas:
1. Serão desprezadas variações salariais na simulação. Ou seja: assume-se de que o mesmo salário será recebido do primeiro ao último dia de contribuição.
2. Em decorrência do item _1_, não será necessário calcular os 80% salários mais altos do contribuinte para o modelo presente de previdência.
3. Serão desprezados os casos excepcionais de cálculo previdenciário (pessoas com deficiência, professores, policiais, trabalhadores expostos a risco etc).
4. Os cálculos levarão em conta apenas aposentadorias segundo o Regime Geral de Previdência.
5. Como não estão claras as regras de rendimento do modelo de capitalização, assume-se que o rendimento padrão do fundo de aposentadoria individual do modelo novo de aposentadoria será de 100% do CDI - que à data de publicação deste software, era de 6,4% ao ano. No entanto, se o usuário assim preferir, ele poderá inserir uma taxa de rendimento customizada.
6. Taxas de inflação e correção monetária serão desprezadas - assume-se que, do momento do cálculo até o momento da aposentadoria, não haverá inflação, para efeitos de simplificação do cálculo.
7. O cálculo de rendimento da aposentadoria (para o novo modelo previdenciário) levará em conta a fórmula de _Valor Futuro_ dos juros compostos, considerando aporte mensal equivalente à contribuição previdenciária devida. 
8. Entendemos que ambos os modelos previdenciários são trans-excludentes. Portanto, desde já pedimos desculpas pela abordagem binária dos parâmetros de entrada (gênero).
9. O Cálculo de Fator Previdenciário levará em consideração a expectativa de vida do brasileiro como de [76 anos](https://www.correiobraziliense.com.br/app/noticia/brasil/2018/07/25/interna-brasil,697305/expectativa-de-vida-do-brasileiro-chega-a-76-anos-a-maior-da-historia.shtml).

## Cálculo (Modelo Solidário)

### Parâmetros

* Tempo de Contribuição
* Idade de Aposentadoria
* Salário de Contribuinte
* Gênero

### Fórmula

_SA_ = (_SC_ * 0,8) * _FP_

Onde:
* _SC_: Salário de Contribuinte
* _FP_: Fator previdenciário
* _SA_: Salário de Aposentado

Considera-se que o _SA_ será recebido pelo restante da vida do aposentado.

## Cálculo (Modelo Capitalização)

### Parâmetros

* Tempo de Contribuição
* Idade de Aposentadoria
* Salário de Contribuinte
* Gênero
* Taxa de Rendimento Anual do Fundo Previdenciário (assume-se, por padrão, 6,4%)

### Fórmula

_SA_ = (_SC_ * (0,6 + (_AE_ * 0,02))

Onde:
* _SA_: Salário de Aposentado
* _SC_: Salário de Contribuinte
* _AE_: Anos Excedentes (aos 20 anos mínimos de contribuição)

Considera-se que o _SA_ será recebido até o esgotamento do Montante do Fundo de Aposentadoria.
O Fundo de Aposentadoria será calculado com fórmula de valor futuro com aporte mensal, da matemática financeira. E será informado ao final do cálculo, juntamente com quantos anos até que o _SA_ deixe de ser recebido, para evidenciar o desamparo no caso de sobrevida do aposentado.

# Descritivo dos Modelos de Aposentadoria

## Modelo Presente de Aposentadoria

O presente modelo de aposentadoria, dito "solidário", de forma resumida, consiste em os trabalhadores que hoje estão ativos no mercado (juntamente com contribuição governamental e do empregador) financiando a aposentadoria de quem está aposentado hoje (além de outras contribuições e impostos que servem como receita para o sistema previdenciário).

Algumas das regras do presente modelo de aposentadoria estão descritas nas subseções seguintes.

### Idade

No modelo atual, há uma idade mínima de aposentadoria que é de:
* Homens: 65 anos
* Mulheres: 60 anos

_Válido apenas para quem contribuiu por pelo menos 15 anos com a previdência_ 

### Tempo de Contribuição Previdenciária

* Homens: 35 anos
* Mulheres: 30 anos

_O valor da aposentadoria, nesses casos, pode sofrer redução devido ao **fator previdenciário** caso a pessoa aposentada não atenda ao critério de aposentadoria por idade_

### Exceções

#### Camponeses

A **idade mínima** de aposentadoria para camponeses é diferente:
* Homens: 60 anos
* Mulheres: 55 anos

Além disso, para camponeses se aposentarem por idade, não é exigido o mínimo de 15 anos de contribuição.

#### Aposentadorias Especiais

Algumas categorias de trabalhadores possuem regras especiais para aposentadoria, particulares a cada uma dessas categorias, por conta de exposição contínua a riscos (de vida, saúde etc), como é o caso de professores e policiais.

### Como a Contribuição Previdenciária Afeta o Valor da Aposentadoria?

Se é certo que as pessoas não recebem o mesmo salário ao longo da vida inteira, então, como saber quanto receberão ao se aposentar? O que é levado em conta?

#### Fator Previdenciário

O fator previdenciário é uma alteração no cálculo de valor de aposentadoria que faz com que pessoas que já atingiram o tempo de contribuição previdenciária (35/30 anos) tenham redução no valor de aposentadoria de acordo com o quão jovens são ao se aposentar - quanto mais jovem, maior a redução no valor de aposentadoria. Para alguns poucos casos, o fator previdenciário pode aumentar o salário de aposentadoria.

O Fator Previdenciário se aplica às aposentadorias que não atendem ao critério de aposentadoria por idade, aplicando-se a esse último grupo apenas nos casos em que aumenta o salário de aposentado.

O cálculo do fator previdenciário se dá da seguinte maneira:

![Cálculo do fator previdenciário](https://www.ieprev.com.br/assets/wise/images/formulafator/formula_fator.gif)

Para que o fator previdenciário não afete negativamente o salário do aposentado, é necessário que ele atenda à **regra 85/95**, que, em resumo, determina que, se _i_ + _tc_ >= _85_ (para mulheres) ou _i_ + _tc_ >= _95_ (para homens), sendo _i_ a sua idade, e _tc_ o tempo de contribuição, então o fator previdenciário não irá incidir sobre seu salário de aposentado - novamente, assumindo que o aposentado não atende aos critérios de aposentadoria por idade. 

#### Salário de Benefício

Ao salário de aposentadoria, antes da aplicação de quaisquer modificadores (mas depois de aplicar o fator previdenciário), é dado o nome de _Salário de Benefício_, ao qual são aplicados diferentes modificadores a depender se a aposentadoria se dará por idade ou se por tempo de contribuição.

Para quem trabalha no Regime Geral, é feita uma média de 80% das contribuições de maior valor feitas para a previdência - ou seja, dos salários que você recebeu ao longo de sua vida como contribuinte, os 20% menores salários que você já recebeu não serão levados em conta para calcular seu salário de Benefício.

No caso de aposentadoria por idade, o salário de aposentadoria será 70% do salário de benefício, acrescido de 1% para cada ano de trabalho, até o limite de 100%.

Para este simulador, desprezaremos os casos de tempo total apurado proporcional. Mas, caso queira compreender melhor a matemática do cálculo de salário de aposentadoria, basta acessar a [explicação do próprio INSS](https://www.inss.gov.br/beneficios/aposentadoria-por-tempo-de-contribuicao/valor-das-aposentadorias/).

### Teto do Valor da Aposentadoria

Há um valor máximo pago pela previdência aos aposentados (regime geral). Em 2018 houve a fixação desse valor em R$ 5.645,80 (sujeito à correção com base no INPC).

### Alíquotas de Contribuição:

Abaixo, uma tabela das alíquotas de contribuição (apenas para Regime Geral):


#### Tabela de Alíquotas do Modelo Presente
De|A|Alíquota
---|---|---
0|R$ 1.751,81|8%
R$ 1.751,82|R$ 2.919,72|9%
R$ 2.919,73|-|11%

## Modelo Proposto

O novo modelo de aposentadoria, proposto pelo governo Bolsonaro, em 2019, funciona através de sistema de capitalização. Suas regras estão dispersas na [página do Governo Federal](http://www.brasil.gov.br/novaprevidencia). A íntegra da proposta pode ser acessada [aqui](http://www.brasil.gov.br/novaprevidencia/entenda-a-proposta/integra-da-proposta/pec-6-2019.pdf).

Em relação o modo de financiamento da previdência, a novidade do modelo proposto é que o financiamento deixe de ser solidário (coletivo) para ser individual, onde cada trabalhador é responsável pelo montante e o rendimento de seu próprio fundo de aposentadoria. Ou seja, em vez de os trabalhadores ativos financiarem o salário dos aposentados (além da contribuição empresarial e do governo), o fundo de aposentadoria contará apenas com as contribuições do trabalhador ao longo de sua vida contribuinte, aplicado uma taxa de rendimento que ainda não está clara qual será.

Conclui-se, portanto, que não há garantia de que o fundo de aposentadoria do trabalhador será capaz de financiar sua aposentadoria para o resto de sua vida pois, se a vida do trabalhador exceder a capacidade de pagamento do fundo, ele ficará desamparado. Essa conclusão baseia-se na falta de clareza da proposta integral sobre 4 pontos:
* Que instituição irá gerir os fundos de aposentadoria?
* Quem pagará a aposentadoria?
* Se o trabalhador viver o suficiente para o montante acumulado das suas contribuições zerar, ele continuará recebendo o salário de aposentado?
* Se sim, de onde sairão os recursos para pagar essa aposentadoria?  

### Idade 

No modelo proposto, as idades de aposentadoria mudam da seguinte maneira:
* Homens: 65 anos
* Mulheres: 62 anos

_Válido apenas para quem contribuiu por pelo menos 20 anos com a previdência_

Essas regras variam para algumas categorias especiais, como professores (60 anos), pessoas com deficiência, com condição incapacitante permanente e vítimas de acidentes de trabalho. 

### Tempo de Contribuição Previdenciária

Não há mais a opção de aposentadoria por tempo de contribuição. A aposentadoria só estará acessível pelo critério de idade, com tempo de contribuição mínima.

Nos casos excepcionais, como de pessoas com deficiência e condição incapacitante permanente ou acidente de trabalho, a aposentadoria ainda pode ser obtida só com tempo de contribuição.

### Valor da Aposentadoria

Para efeitos de cálculo, a aposentadoria será de 60% da média de todos os salários recebidos ao longo da vida como contribuinte, com um fator de acréscimo de 2% por cada ano contribuindo além dos 20 anos mínimos de contribuição.

Novamente, há regras excepcionais para trabalhadores com deficiência e incapacidade permanente e sujeitos a riscos em ambiente de trabalho.

### Alíquotas de Contribuição:

Abaixo, uma tabela das alíquotas de contribuição (apenas para Regime Geral):

#### Tabela de Alíquotas do Modelo Novo
De|A|Alíquota|Considerado para o aplicativo
---|---|---|---
0|1 s.m.|7,5%|7,5%
1 s.m.|R$ 2.000|7,5% a 8,25%|8,25%
R$ 2000,01|R$ 3.000|8,25% a 9,5%|9,5%
R$ 3000,01|R$ 4.500|9,5% a 11,68%|11,68%
R$ 4500,01| - | 11,68% a 14% |14%