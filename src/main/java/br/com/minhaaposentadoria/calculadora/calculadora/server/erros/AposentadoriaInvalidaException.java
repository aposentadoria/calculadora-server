package br.com.minhaaposentadoria.calculadora.calculadora.server.erros;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.BAD_REQUEST, reason="Você não pode se aposentar com os parâmetros especificados.")
public class AposentadoriaInvalidaException extends Exception {

	private static final long serialVersionUID = -8252335069512929432L;

}
