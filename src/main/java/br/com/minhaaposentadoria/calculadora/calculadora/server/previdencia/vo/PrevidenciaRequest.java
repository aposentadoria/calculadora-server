package br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo;

import java.math.BigDecimal;

import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Genero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PrevidenciaRequest {
	
	private BigDecimal salario;
	
	private int idadeAposentadoria;
	
	private int anosContribuicao;
	
	private Genero genero;
	
	private BigDecimal taxaRendimentoFundoPrevidenciario;

}
