package br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo;

import java.math.BigDecimal;

import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Genero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PrevidenciaResponse {
	
	private BigDecimal salario;
	
	private int idadeAposentadoria;
	
	private int anosContribuicao;
	
	private Genero genero;
	
	private BigDecimal contribuicaoMensal;
	
	private BigDecimal montanteContribuicaoPrevidenciaria;
	
	private BigDecimal salarioAposentadoria;
	
	private BigDecimal anosAmparo;
	
	public String toMarkDownTableRowCapitalizacao() {
		return String.format("%s|%s|%s|%s|%s|%s|%s|%s", genero, salario, idadeAposentadoria, anosContribuicao, contribuicaoMensal, montanteContribuicaoPrevidenciaria, salarioAposentadoria, anosAmparo);
	}
	
	public static String markDownTableHeaderCapitalizacao() {
		return "Genero|Salario|Idade de Aposentadoria|Anos de Contribuicao|Contribuicao Mensal|Montante Fundo Previdenciario|Salario Aposentadoria|Anos de Amparo \n---|---|---|---|---|---|---|---";
	}
	
	public String toMarkDownTableRowSolidario() {
		return String.format("%s|%s|%s|%s|%s|%s", genero, salario, idadeAposentadoria, anosContribuicao, contribuicaoMensal, salarioAposentadoria);
	}
	
	public static String markDownTableHeaderSolidario() {
		return "Genero|Salario|Idade de Aposentadoria|Anos de Contribuicao|Contribuicao Mensal|Salario Aposentadoria \n---|---|---|---|---|---";
	}

}
