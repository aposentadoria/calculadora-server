package br.com.minhaaposentadoria.calculadora.calculadora.server.uteis;

import java.math.BigDecimal;

public enum Constantes {
	;
	public static final BigDecimal TAXA_CDI_04_2019_MENSAL = new BigDecimal("0.005183");
	public static final BigDecimal TAXA_CDI_04_2019_ANUAL = new BigDecimal("0.064");
	public static final int EXPECTATIVA_DE_VIDA = 76;
	
	public enum Capitalizacao {
		;
		public enum Aliquotas {
			;
			public static final BigDecimal FAIXA1 = new BigDecimal("0.075"); 
			public static final BigDecimal FAIXA2 = new BigDecimal("0.0825"); 
			public static final BigDecimal FAIXA3 = new BigDecimal("0.095"); 
			public static final BigDecimal FAIXA4 = new BigDecimal("0.1168"); 
			public static final BigDecimal FAIXA5 = new BigDecimal("0.14"); 
		}
		public enum LimitesSalariais {
			;
			public static final BigDecimal FAIXA1 = new BigDecimal("998");
			public static final BigDecimal FAIXA2 = new BigDecimal("2000");
			public static final BigDecimal FAIXA3 = new BigDecimal("3000");
			public static final BigDecimal FAIXA4 = new BigDecimal("4500");
		}
		public static final int TEMPO_MINIMO_CONTRIBUICAO_ANOS = 20;
		public static final int IDADE_MINIMA_APOSENTADORIA_FEMININA = 62;
		public static final int IDADE_MINIMA_APOSENTADORIA_MASCULINA = 65;
		public static final BigDecimal FATOR_BASICO_CALCULO_SALARIO = new BigDecimal("0.6");
		public static final BigDecimal FATOR_ADICIONAL_CALCULO_SALARIO = new BigDecimal("0.02");
	}
	
	public enum Solidario {
		;
		public enum Aliquotas {
			;
			public static final BigDecimal FAIXA1 = new BigDecimal("0.08"); 
			public static final BigDecimal FAIXA2 = new BigDecimal("0.09"); 
			public static final BigDecimal FAIXA3 = new BigDecimal("0.11"); 
		}
		public enum LimitesSalariais {
			;
			public static final BigDecimal FAIXA1 = new BigDecimal("1751.81");
			public static final BigDecimal FAIXA2 = new BigDecimal("2919.72");
		}
		public static final int TEMPO_MINIMO_CONTRIBUICAO_ANOS = 15;
		public static final int TEMPO_CONTRIBUICAO_HABILITANTE_MASCULINO_ANOS = 35;
		public static final int TEMPO_CONTRIBUICAO_HABILITANTE_FEMININO_ANOS = 30;
		public static final int IDADE_MINIMA_APOSENTADORIA_FEMININA = 60;
		public static final int IDADE_MINIMA_APOSENTADORIA_MASCULINA = 65;
		public static final int IDADE_8595_FEMININA = 86;
		public static final int IDADE_8595_MASCULINA = 96;
		public static final BigDecimal FATOR_BASICO_CALCULO_SALARIO = new BigDecimal("0.70");
		public static final BigDecimal TETO_INSS = new BigDecimal("5839.45");
	}

}
