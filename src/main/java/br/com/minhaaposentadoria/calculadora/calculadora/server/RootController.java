package br.com.minhaaposentadoria.calculadora.calculadora.server;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class RootController {

	@GetMapping
	public ResponseEntity<Void> get() {
		return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).header("Location", "/swagger-ui.html").build();
	}
	
}
