package br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.minhaaposentadoria.calculadora.calculadora.server.erros.AposentadoriaInvalidaException;
import br.com.minhaaposentadoria.calculadora.calculadora.server.matematica.MatematicaFinanceiraService;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo.PrevidenciaRequest;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo.PrevidenciaResponse;
import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Constantes;
import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Genero;

/**
 * 
 * Serviço para cálculo previdenciário segundo proposta de reforma da previdência.
 * 
 * @author ramonfacchin
 *
 */
@Service
public class PrevidenciaCapitalizacaoService {
	
	@Autowired
	MatematicaFinanceiraService matematicaFinanceiraService;
	
	public PrevidenciaCapitalizacaoService() {
	}
	
	public PrevidenciaCapitalizacaoService(MatematicaFinanceiraService mtm) {
		this.matematicaFinanceiraService = mtm;
	}
	
	/**
	 * @param salario salário do trabalhador
	 * @return alíquota de cobrança da contribuição previdenciária com base no salário.
	 */
	public BigDecimal inferirAliquotaContribuicaoPorSalario(BigDecimal salario) {
		if (salario.compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA1) <= 0) {
			return Constantes.Capitalizacao.Aliquotas.FAIXA1;
		}
		if (salario.compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA1) > 0
				&& salario.compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA2) <= 0) {
			return Constantes.Capitalizacao.Aliquotas.FAIXA2;
		}
		if (salario.compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA2) > 0
				&& salario.compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA3) <= 0) {
			return Constantes.Capitalizacao.Aliquotas.FAIXA3;
		}
		if (salario.compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA3) > 0
				&& salario.compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA4) <= 0) {
			return Constantes.Capitalizacao.Aliquotas.FAIXA4;
		}
		return Constantes.Capitalizacao.Aliquotas.FAIXA5;
	}
	
	/**
	 * @param salario salário do trabalhador
	 * @return contribuição mensal para previdência, com base no salário recebido.
	 */
	public BigDecimal calcularContribuicaoMensal(BigDecimal salario) {
		return salario.multiply(inferirAliquotaContribuicaoPorSalario(salario));
	}
	
	/**
	 * @param salario média salarial do trabalhador
	 * @param anosContribuicao quantidade de anos que o trabalhador contribuiu com a previdência
	 * @param taxaRendimento taxa de rendimento sobre a contribuição previdenciária acumulada
	 * @return montante de dinheiro contido no fundo previdenciário no momento da aposentadoria
	 */
	public BigDecimal calcularMontanteContribuicaoPrevidenciaria(BigDecimal salario, int anosContribuicao, BigDecimal taxaRendimento) {
		if (taxaRendimento == null) {
			taxaRendimento = Constantes.TAXA_CDI_04_2019_MENSAL;
		}
		return matematicaFinanceiraService.calcularInvestimentoAporteFixoEntradaZero(calcularContribuicaoMensal(salario), taxaRendimento, (anosContribuicao*12));
	}
	
	/**
	 * @param salario média salarial da vida de contribuinte
	 * @param anosContribuicao quantidade de anos que o trabalhador contribuiu com a previdência
	 * @param genero MASCULINO/FEMININO
	 * @param idadeAposentadoria idade com que o trabalhador se aposentou
	 * @return o salário a ser recebido durante a aposentadoria
	 * @throws AposentadoriaInvalidaException caso os parâmetros de aposentadoria não sejam válidos
	 */
	public BigDecimal calcularSalarioAposentadoria(BigDecimal salario, int anosContribuicao, Genero genero, int idadeAposentadoria) throws AposentadoriaInvalidaException {
		int anosDeContribuicaoExcedentes = anosContribuicao - Constantes.Capitalizacao.TEMPO_MINIMO_CONTRIBUICAO_ANOS;
		BigDecimal fatorSalarioAposentadoria = Constantes.Capitalizacao.FATOR_BASICO_CALCULO_SALARIO;
		if (anosDeContribuicaoExcedentes > 0) {
			if (anosDeContribuicaoExcedentes >= 40) {
				fatorSalarioAposentadoria = BigDecimal.ONE;
			} else {
				fatorSalarioAposentadoria = Constantes.Capitalizacao.FATOR_BASICO_CALCULO_SALARIO.add(new BigDecimal(anosDeContribuicaoExcedentes).multiply(Constantes.Capitalizacao.FATOR_ADICIONAL_CALCULO_SALARIO));
			}
		}
		return salario.multiply(fatorSalarioAposentadoria);
	}
	
	/**
	 * 
	 * @param salario média salarial da vida de contribuinte
	 * @param anosContribuicao quantidade de anos que o trabalhador contribuiu com a previdência
	 * @param genero MASCULINO/FEMININO
	 * @param idadeAposentadoria idade com que o trabalhador se aposentou
	 * @param taxaRendimento taxa de rendimento do fundo de aposentadoria do trabalhador
	 * @return quantos anos o trabalhador possuirá de cobertura previdenciária
	 * @throws AposentadoriaInvalidaException caso os parâmetros de aposentadoria não sejam válidos
	 */
	public BigDecimal calcularAnosDeAmparo(BigDecimal salario, Genero genero, int idadeAposentadoria, int anosContribuicao, BigDecimal taxaRendimento) throws AposentadoriaInvalidaException {
		BigDecimal salarioAposentadoria = calcularSalarioAposentadoria(salario, anosContribuicao, genero, idadeAposentadoria);
		BigDecimal montanteContribuicaoPrevidenciaria = calcularMontanteContribuicaoPrevidenciaria(salarioAposentadoria, anosContribuicao, taxaRendimento);
		return calcularAnosDeAmparo(salarioAposentadoria, montanteContribuicaoPrevidenciaria);
	}
	
	/**
	 * 
	 * @param salarioAposentadoria salário do aposentado
	 * @param montanteContribuicaoPrevidenciaria quanto dinheiro foi acumulado no fundo previdenciário do trabalhador
	 * @return quantos anos o trabalhador possuirá de cobertura previdenciária
	 * @throws AposentadoriaInvalidaException caso os parâmetros de aposentadoria não sejam válidos
	 */
	public BigDecimal calcularAnosDeAmparo(BigDecimal salarioAposentadoria, BigDecimal montanteContribuicaoPrevidenciaria) {
		return montanteContribuicaoPrevidenciaria.divide(salarioAposentadoria, 2, RoundingMode.FLOOR).divide(BigDecimal.valueOf(12L), 2, RoundingMode.FLOOR);
	}
	
	/**
	 * @param requisicao parâmetros da vida de contribuinte do trabalhador
	 * @return as condições de aposentadoria do trabalhador com os parâmetros informados
	 * @throws AposentadoriaInvalidaException caso os parâmetros de aposentadoria não sejam válidos
	 */
	public PrevidenciaResponse emitirRelatorioAposentadoria(PrevidenciaRequest requisicao) throws AposentadoriaInvalidaException{
		validarAposentadoria(requisicao);
		BigDecimal contribuicaoMensal = calcularContribuicaoMensal(requisicao.getSalario());
		BigDecimal montanteContribuicaoPrevidenciaria = calcularMontanteContribuicaoPrevidenciaria(requisicao.getSalario(), requisicao.getAnosContribuicao(), requisicao.getTaxaRendimentoFundoPrevidenciario());
		BigDecimal salarioAposentadoria = calcularSalarioAposentadoria(requisicao.getSalario(), requisicao.getAnosContribuicao(), requisicao.getGenero(), requisicao.getIdadeAposentadoria());
		BigDecimal anosAmparo = calcularAnosDeAmparo(salarioAposentadoria, montanteContribuicaoPrevidenciaria);
		
		return PrevidenciaResponse.builder()
				.anosAmparo(anosAmparo.setScale(2, RoundingMode.FLOOR))
				.anosContribuicao(requisicao.getAnosContribuicao())
				.idadeAposentadoria(requisicao.getIdadeAposentadoria())
				.contribuicaoMensal(contribuicaoMensal.setScale(2, RoundingMode.FLOOR))
				.salario(requisicao.getSalario())
				.genero(requisicao.getGenero())
				.montanteContribuicaoPrevidenciaria(montanteContribuicaoPrevidenciaria.setScale(2, RoundingMode.FLOOR))
				.salarioAposentadoria(salarioAposentadoria.setScale(2, RoundingMode.FLOOR))
				.build();
	}
	
	/**
	 * @param requisicao parâmetros de aposentadoria do trabalhador
	 * @throws AposentadoriaInvalidaException caso os parâmetros de aposentadoria não sejam válidos
	 */
	private void validarAposentadoria(PrevidenciaRequest requisicao) throws AposentadoriaInvalidaException {
		if (requisicao == null || requisicao.getGenero() == null || requisicao.getSalario() == null || requisicao.getAnosContribuicao() < 0 || requisicao.getIdadeAposentadoria() < 0 || requisicao.getSalario().compareTo(BigDecimal.ZERO) <= 0) {
			throw new AposentadoriaInvalidaException();
		}
		validarAposentadoria(requisicao.getIdadeAposentadoria(), requisicao.getAnosContribuicao(), requisicao.getGenero());
	}
	
	/**
	 * @param idadeAposentadoria idade com que o trabalhador se aposentará
	 * @param anosContribuicao quantos anos o trabalhador contribuirá
	 * @param genero MASCULINO/FEMININO
	 * @throws AposentadoriaInvalidaException caso os parâmetros de aposentadoria não sejam válidos
	 */
	private void validarAposentadoria(int idadeAposentadoria, int anosContribuicao, Genero genero) throws AposentadoriaInvalidaException {
		switch (genero) {
		case FEMININO:
			if (idadeAposentadoria < Constantes.Capitalizacao.IDADE_MINIMA_APOSENTADORIA_FEMININA) {
				throw new AposentadoriaInvalidaException();
			}
			break;
		case MASCULINO:
			if (idadeAposentadoria < Constantes.Capitalizacao.IDADE_MINIMA_APOSENTADORIA_MASCULINA) {
				throw new AposentadoriaInvalidaException();
			}
			break;
		}
		if (anosContribuicao < Constantes.Capitalizacao.TEMPO_MINIMO_CONTRIBUICAO_ANOS) {
			throw new AposentadoriaInvalidaException();
		}
	}

}
