package br.com.minhaaposentadoria.calculadora.calculadora.server.matematica;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

/**
 * Serviço com operações necessárias de matemática financeira.
 * 
 * @author ramonfacchin
 *
 */
@Service
public class MatematicaFinanceiraService {
	
	/**
	 * Calcula o valor futuro de uma aplicação, aplicando juros compostos.
	 * @param valorPresente o valor de depósito
	 * @param taxaDeJuros a taxa de rendimento
	 * @param vezes quantos períodos de rendimento da taxa informada
	 * @return {@link BigDecimal} com o valor futuro da aplicação.
	 */
	public BigDecimal calcularValorFuturo(BigDecimal valorPresente, BigDecimal taxaDeJuros, int vezes) {
		return valorPresente.multiply(taxaDeJuros.add(BigDecimal.ONE).pow(vezes));
	}
	
	/**
	 * Calcula o valor futuro de uma aplicação periódica fixa, com valor inicial zero.
	 * @param aporte valor a ser acrescentado ao fundo a cada período.
	 * @param taxaDeJuros taxa de rendimento.
	 * @param periodos quantos períodos de rendimento da taxa informada
	 * @return {@link BigDecimal} com o valor futuro da aplicação.
	 */
	public BigDecimal calcularInvestimentoAporteFixoEntradaZero(BigDecimal aporte, BigDecimal taxaDeJuros, int periodos) {
		return calcularInvestimentoAporteFixo(BigDecimal.ZERO, aporte, taxaDeJuros, periodos);
	}
	
	/**
	 * Calcula o valor futuro de uma aplicação periódica fixa.
	 * @param aporte valor inicial do fundo.
	 * @param aporte valor a ser acrescentado ao fundo a cada período.
	 * @param taxaDeJuros taxa de rendimento.
	 * @param periodos quantos períodos de rendimento da taxa informada
	 * @return {@link BigDecimal} com o valor futuro da aplicação.
	 */
	public BigDecimal calcularInvestimentoAporteFixo(BigDecimal valorInicial, BigDecimal aporte, BigDecimal taxaDeJuros, int periodos) {
		BigDecimal montante = BigDecimal.ZERO.add(valorInicial);
		for (int i = 0; i < periodos; i++) {
			montante = montante.add(aporte);
			montante = calcularValorFuturo(montante, taxaDeJuros, 1);
		}
		return montante;
	}

}
