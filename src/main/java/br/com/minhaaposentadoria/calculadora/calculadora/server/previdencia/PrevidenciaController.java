package br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.minhaaposentadoria.calculadora.calculadora.server.erros.AposentadoriaInvalidaException;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo.PrevidenciaRequest;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo.PrevidenciaResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags={"/previdencia"},description="API de cálculo previdenciário.")
@RestController
@RequestMapping("/previdencia")
public class PrevidenciaController {
	
	@Autowired
	PrevidenciaCapitalizacaoService capitalizacaoService;
	
	@Autowired
	PrevidenciaSolidariaService solidariaService;
	
	@GetMapping(path="/status")
	public String status() {
		return "OK";
	}

	@ApiOperation(value="Realiza o cálculo previdenciário no sistema de repartição.",consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(path="/solidaria", consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public PrevidenciaResponse calcularPrevidenciaSolidaria(@RequestBody PrevidenciaRequest request) throws AposentadoriaInvalidaException {
		return solidariaService.emitirRelatorioAposentadoria(request);
	}
	
	@ApiOperation(value="Realiza o cálculo previdenciário no sistema de capitalização.",consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(path="/capitalizacao", consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public PrevidenciaResponse calcularPrevidenciaCapitalizacao(@RequestBody PrevidenciaRequest request) throws AposentadoriaInvalidaException {
		return capitalizacaoService.emitirRelatorioAposentadoria(request);
	}

}
