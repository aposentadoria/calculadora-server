package br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import br.com.minhaaposentadoria.calculadora.calculadora.server.erros.AposentadoriaInvalidaException;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo.PrevidenciaRequest;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo.PrevidenciaResponse;
import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Constantes;
import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Genero;

@Service
public class PrevidenciaSolidariaService {
	
	public PrevidenciaSolidariaService() {
	}
	
	public BigDecimal calcularContribuicaoMensal(BigDecimal salario) {
		return salario.multiply(inferirAliquotaContribuicaoPorSalario(salario));
	}
	
	public BigDecimal calcularFatorPrevidenciario(BigDecimal aliquota, int anosContribuicao, int idadeAposentadoria, Genero genero) {
		/*
		 * termo1: (anosContribuicao * aliquota) / expectativaSobrevida
		 * sendo: expectativaSobrevida = expectativa de vida - idadeAposentadoria
		 * 
		 * termo2: 1 + ( (idadeAposentadoria + anosContribuicao * aliquota) / 100 )
		 * 
		 * fatorPrevidenciario = termo1 * termo2
		 * 
		 * se atingir regra 85/95: não aplicar
		 * se idade >= 65 && genero == MASCULINO: não aplicar
		 * se idade >= 60 && genero == FEMININO: não aplicar
		 */
		BigDecimal fatorPrevidenciario = null;
		
		BigDecimal anosContribuicaoAliquota = aliquota.multiply(new BigDecimal(anosContribuicao));
		BigDecimal termo1 = anosContribuicaoAliquota.divide(new BigDecimal(Constantes.EXPECTATIVA_DE_VIDA - idadeAposentadoria), 2, RoundingMode.FLOOR);
		BigDecimal termo2 = anosContribuicaoAliquota.add(new BigDecimal(idadeAposentadoria)).divide(new BigDecimal(100), 2, RoundingMode.FLOOR).add(BigDecimal.ONE);
		fatorPrevidenciario = termo1.multiply(termo2);
		
		if (fatorPrevidenciario.compareTo(BigDecimal.ONE) < 0) {
			// Fator previdenciário desvantajoso, verificar se pode ser ignorado
			
			// Idade superior à idade mínima de aposentadoria?
			switch (genero) {
			case FEMININO:
				if (idadeAposentadoria >= 60) {
					return BigDecimal.ONE;
				}
				break;
			case MASCULINO:
			default:
				if (idadeAposentadoria >= 65) {
					return BigDecimal.ONE;
				}
				break;
			}
			
			// Atende à regra 85/95?
			if (atendeRegra8595(genero, idadeAposentadoria, anosContribuicao)) {
				return BigDecimal.ONE;
			}
		}
		return fatorPrevidenciario;
	}
	
	boolean atendeRegra8595(Genero genero, int idadeAposentadoria, int anosContribuicao) {
		switch (genero) {
		case FEMININO:
			return idadeAposentadoria + anosContribuicao >= Constantes.Solidario.IDADE_8595_FEMININA;
		case MASCULINO:
		default:
			return idadeAposentadoria + anosContribuicao >= Constantes.Solidario.IDADE_8595_MASCULINA;
		}
	}
	
	public BigDecimal inferirAliquotaContribuicaoPorSalario(BigDecimal salario) {
		if (salario.compareTo(Constantes.Solidario.LimitesSalariais.FAIXA1) <= 0) {
			return Constantes.Solidario.Aliquotas.FAIXA1;
		}
		if (salario.compareTo(Constantes.Solidario.LimitesSalariais.FAIXA1) > 0
				&& salario.compareTo(Constantes.Solidario.LimitesSalariais.FAIXA2) <= 0) {
			return Constantes.Solidario.Aliquotas.FAIXA2;
		}
		return Constantes.Solidario.Aliquotas.FAIXA3;
	}
	
	public BigDecimal calcularSalarioAposentadoria(BigDecimal salario, int anosContribuicao, Genero genero, int idadeAposentadoria) throws AposentadoriaInvalidaException {
		validarAposentadoria(idadeAposentadoria, anosContribuicao, genero);
		boolean isAposentadoriaPorTempoContribuicao = idadeAposentadoria < (Genero.FEMININO.equals(genero) ? Constantes.Solidario.IDADE_MINIMA_APOSENTADORIA_FEMININA : Constantes.Solidario.IDADE_MINIMA_APOSENTADORIA_MASCULINA);
		BigDecimal aliquota = inferirAliquotaContribuicaoPorSalario(salario);
		BigDecimal fatorPrevidenciario = calcularFatorPrevidenciario(aliquota , anosContribuicao, idadeAposentadoria, genero);
		
		if (!isAposentadoriaPorTempoContribuicao) {
			BigDecimal fatorSalarioAposentadoria = Constantes.Solidario.FATOR_BASICO_CALCULO_SALARIO.add(new BigDecimal("0.1").multiply(new BigDecimal(anosContribuicao)));
			if (fatorSalarioAposentadoria.compareTo(BigDecimal.ONE) > 0) {
				fatorSalarioAposentadoria = BigDecimal.ONE;
			}
			salario = salario.multiply(fatorSalarioAposentadoria);
		}
		
		return salario.multiply(fatorPrevidenciario);
	}
	
	public PrevidenciaResponse emitirRelatorioAposentadoria(PrevidenciaRequest requisicao) throws AposentadoriaInvalidaException{
		validarAposentadoria(requisicao);
		BigDecimal contribuicaoMensal = calcularContribuicaoMensal(requisicao.getSalario());
		BigDecimal salarioAposentadoria = calcularSalarioAposentadoria(requisicao.getSalario(), requisicao.getAnosContribuicao(), requisicao.getGenero(), requisicao.getIdadeAposentadoria());
		
		return PrevidenciaResponse.builder()
				.anosContribuicao(requisicao.getAnosContribuicao())
				.idadeAposentadoria(requisicao.getIdadeAposentadoria())
				.contribuicaoMensal(contribuicaoMensal.setScale(2, RoundingMode.FLOOR))
				.salario(requisicao.getSalario())
				.genero(requisicao.getGenero())
				.salarioAposentadoria(salarioAposentadoria.setScale(2, RoundingMode.FLOOR))
				.build();
	}
	
	private void validarAposentadoria(PrevidenciaRequest requisicao) throws AposentadoriaInvalidaException {
		if (requisicao == null || requisicao.getGenero() == null || requisicao.getSalario() == null || requisicao.getAnosContribuicao() < 0 || requisicao.getIdadeAposentadoria() < 0 || requisicao.getSalario().compareTo(BigDecimal.ZERO) <= 0) {
			throw new AposentadoriaInvalidaException();
		}
		validarAposentadoria(requisicao.getIdadeAposentadoria(), requisicao.getAnosContribuicao(), requisicao.getGenero());
	}
	
	private void validarAposentadoria(int idadeAposentadoria, int anosContribuicao, Genero genero) throws AposentadoriaInvalidaException {
		int tempoHabilitante = Genero.FEMININO.equals(genero) ? Constantes.Solidario.TEMPO_CONTRIBUICAO_HABILITANTE_FEMININO_ANOS : Constantes.Solidario.TEMPO_CONTRIBUICAO_HABILITANTE_MASCULINO_ANOS;
		int idadeMinima = Genero.FEMININO.equals(genero) ? Constantes.Solidario.IDADE_MINIMA_APOSENTADORIA_FEMININA : Constantes.Solidario.IDADE_MINIMA_APOSENTADORIA_MASCULINA;
		
		if (anosContribuicao < tempoHabilitante) {
			if (idadeAposentadoria < idadeMinima) {
				throw new AposentadoriaInvalidaException();
			}
		}
	}
	
}
