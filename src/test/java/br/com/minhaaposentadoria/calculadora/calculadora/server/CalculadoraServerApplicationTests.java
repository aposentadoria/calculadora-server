package br.com.minhaaposentadoria.calculadora.calculadora.server;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.minhaaposentadoria.calculadora.calculadora.server.erros.AposentadoriaInvalidaException;
import br.com.minhaaposentadoria.calculadora.calculadora.server.matematica.MatematicaFinanceiraService;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.PrevidenciaSolidariaService;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo.PrevidenciaRequest;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.vo.PrevidenciaResponse;
import br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia.PrevidenciaCapitalizacaoService;
import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Constantes;
import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Genero;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculadoraServerApplicationTests {
	
	@Autowired
	MatematicaFinanceiraService matematicaFinanceiraService;
	
	@Autowired
	PrevidenciaCapitalizacaoService previdenciaCapitalizacaoService;
	
	@Autowired
	PrevidenciaSolidariaService previdenciaSolidariaService;

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testeValorFuturo() throws Exception {
		BigDecimal investimento = new BigDecimal("10000");
		int vezes = 12;
		BigDecimal valorFuturoTaxaMensal = matematicaFinanceiraService.calcularValorFuturo(investimento, Constantes.TAXA_CDI_04_2019_MENSAL, vezes);
		BigDecimal valorFuturoTaxaAnual = matematicaFinanceiraService.calcularValorFuturo(investimento, Constantes.TAXA_CDI_04_2019_ANUAL, 1);
		assertTrue(valorFuturoTaxaMensal.compareTo(valorFuturoTaxaAnual) < 0);
	}
	
	@Test
	public void testeInvetimento() throws Exception {
		BigDecimal aporte = new BigDecimal("800");
		int periodos = 12;
		System.out.println(matematicaFinanceiraService.calcularInvestimentoAporteFixoEntradaZero(aporte, Constantes.TAXA_CDI_04_2019_MENSAL, periodos));
	}
	
	@Test
	public void testeSalarioSemAnosAdicionais() throws Exception {
		BigDecimal salarioAposentadoria = previdenciaCapitalizacaoService.calcularSalarioAposentadoria(new BigDecimal("10000"), 20, Genero.MASCULINO, 65);
		System.out.println(salarioAposentadoria);
	}
	
	@Test
	public void testeSalarioComAnosAdicionais() throws Exception {
		BigDecimal salarioAposentadoria = previdenciaCapitalizacaoService.calcularSalarioAposentadoria(new BigDecimal("10000"), 25, Genero.MASCULINO, 65);
		System.out.println(salarioAposentadoria);
	}
	
	@Test
	public void testeSalarioComAposentadoriaCompleta() throws Exception {
		BigDecimal salarioAposentadoria = previdenciaCapitalizacaoService.calcularSalarioAposentadoria(new BigDecimal("10000"), 40, Genero.MASCULINO, 65);
		System.out.println(salarioAposentadoria);
	}
	
	@Test
	@Ignore
	public void testeEstatisticasAposentadoria() throws Exception {
		BigDecimal salario = new BigDecimal("4000");
		int idadeAposentadoria = 62;
		int anosContribuicao = 37;
		Genero genero = Genero.FEMININO;
		
		BigDecimal contribuicaoMensal = previdenciaCapitalizacaoService.calcularContribuicaoMensal(salario);
		BigDecimal montanteContribuicaoPrevidenciaria = previdenciaCapitalizacaoService.calcularMontanteContribuicaoPrevidenciaria(salario, anosContribuicao, null);
		BigDecimal salarioAposentadoria = previdenciaCapitalizacaoService.calcularSalarioAposentadoria(salario, anosContribuicao, genero, idadeAposentadoria);
		BigDecimal anosAmparo = montanteContribuicaoPrevidenciaria.divide(salarioAposentadoria).round(new MathContext(2, RoundingMode.FLOOR)).divide(BigDecimal.valueOf(12L));
		
		System.out.println(String.format("Um(a) %s com um salário de %s, contribuindo por %s anos e se aposentando aos %s anos, deverá contribuir com %s mensais, acumulando, no momento da aposentadoria, um montante de %s e receberá um salário de %s por %s até o fundo de aposentadoria se esgotar.", genero, salario, anosContribuicao, idadeAposentadoria, contribuicaoMensal, montanteContribuicaoPrevidenciaria.setScale(2, RoundingMode.FLOOR), salarioAposentadoria.setScale(2, RoundingMode.FLOOR), anosAmparo.setScale(2, RoundingMode.FLOOR)));
	}
	
	@Test
	public void testeSimulacoesFaixas() throws Exception {
		List<PrevidenciaRequest> requisicoes = new ArrayList<>();
		PrevidenciaRequest mulherMinimo = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(998L))
				.genero(Genero.FEMININO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(mulherMinimo);
		PrevidenciaRequest mulherMinimoContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(998L))
				.genero(Genero.FEMININO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(mulherMinimoContribMaxima);
		PrevidenciaRequest homemMinimo = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(998L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(homemMinimo);
		PrevidenciaRequest homemMinimoContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(998L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(homemMinimoContribMaxima);
		PrevidenciaRequest mulherFaixa2 = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(2000L))
				.genero(Genero.FEMININO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(mulherFaixa2);
		PrevidenciaRequest mulherFaixa2ContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(2000L))
				.genero(Genero.FEMININO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(mulherFaixa2ContribMaxima);
		PrevidenciaRequest homemFaixa2 = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(2000L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(homemFaixa2);
		PrevidenciaRequest homemFaixa2ContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(2000L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(homemFaixa2ContribMaxima);
		PrevidenciaRequest mulherFaixa3 = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(3000L))
				.genero(Genero.FEMININO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(mulherFaixa3);
		PrevidenciaRequest mulherFaixa3ContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(3000L))
				.genero(Genero.FEMININO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(mulherFaixa3ContribMaxima);
		PrevidenciaRequest homemFaixa3 = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(3000L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(homemFaixa3);
		PrevidenciaRequest homemFaixa3ContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(3000L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(homemFaixa3ContribMaxima);
		PrevidenciaRequest mulherFaixa4 = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(4500L))
				.genero(Genero.FEMININO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(mulherFaixa4);
		PrevidenciaRequest mulherFaixa4ContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(4500L))
				.genero(Genero.FEMININO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(mulherFaixa4ContribMaxima);
		PrevidenciaRequest homemFaixa4 = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(4500L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(homemFaixa4);
		PrevidenciaRequest homemFaixa4ContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(4500L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(homemFaixa4ContribMaxima);
		PrevidenciaRequest mulherFaixa5 = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(7000L))
				.genero(Genero.FEMININO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(mulherFaixa5);
		PrevidenciaRequest mulherFaixa5ContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(7000L))
				.genero(Genero.FEMININO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(mulherFaixa5ContribMaxima);
		PrevidenciaRequest homemFaixa5 = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(7000L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(20)
				.build();
		requisicoes.add(homemFaixa5);
		PrevidenciaRequest homemFaixa5ContribMaxima = PrevidenciaRequest.builder()
				.idadeAposentadoria(65)
				.salario(new BigDecimal(7000L))
				.genero(Genero.MASCULINO)
				.anosContribuicao(40)
				.build();
		requisicoes.add(homemFaixa5ContribMaxima);
		PrevidenciaRequest tais = PrevidenciaRequest.builder()
				.idadeAposentadoria(62)
				.salario(new BigDecimal(4000L))
				.genero(Genero.FEMININO)
				.anosContribuicao(37)
				.build();
		requisicoes.add(tais);
		
		System.out.println(PrevidenciaResponse.markDownTableHeaderCapitalizacao());
		requisicoes.forEach(requisicao -> {
			try {
				PrevidenciaResponse resposta = previdenciaCapitalizacaoService.emitirRelatorioAposentadoria(requisicao);
				System.out.println(resposta.toMarkDownTableRowCapitalizacao());
			} catch (AposentadoriaInvalidaException e) {
				e.printStackTrace();
			}
		});
		
		System.out.println("\n");
		System.out.println("\n");
		System.out.println("\n");
		
		System.out.println(PrevidenciaResponse.markDownTableHeaderSolidario());
		requisicoes.forEach(requisicao -> {
			try {
				PrevidenciaResponse resposta = previdenciaSolidariaService.emitirRelatorioAposentadoria(requisicao);
				System.out.println(resposta.toMarkDownTableRowSolidario());
			} catch (AposentadoriaInvalidaException e) {
				e.printStackTrace();
			}
		});
	}

}
