package br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Constantes;

public class PrevidenciaSolidariaServiceTest {
	
	private PrevidenciaSolidariaService service;
	
	@Before
	public void setUp() throws Exception {
		service = new PrevidenciaSolidariaService();
	}
	
	@Test
	public void testeAliquotas() throws Exception {
		Assert.assertEquals(Constantes.Solidario.Aliquotas.FAIXA1, service.inferirAliquotaContribuicaoPorSalario(BigDecimal.ZERO));
		Assert.assertEquals(Constantes.Solidario.Aliquotas.FAIXA1, service.inferirAliquotaContribuicaoPorSalario(Constantes.Solidario.LimitesSalariais.FAIXA1));
		Assert.assertNotEquals(Constantes.Solidario.Aliquotas.FAIXA1, service.inferirAliquotaContribuicaoPorSalario(Constantes.Solidario.LimitesSalariais.FAIXA2));
		
		Assert.assertNotEquals(Constantes.Solidario.Aliquotas.FAIXA2, service.inferirAliquotaContribuicaoPorSalario(Constantes.Solidario.LimitesSalariais.FAIXA1));
		Assert.assertEquals(Constantes.Solidario.Aliquotas.FAIXA2, service.inferirAliquotaContribuicaoPorSalario(Constantes.Solidario.LimitesSalariais.FAIXA1.add(BigDecimal.ONE)));
		Assert.assertEquals(Constantes.Solidario.Aliquotas.FAIXA2, service.inferirAliquotaContribuicaoPorSalario(Constantes.Solidario.LimitesSalariais.FAIXA2));
		
		Assert.assertNotEquals(Constantes.Solidario.Aliquotas.FAIXA3, service.inferirAliquotaContribuicaoPorSalario(Constantes.Solidario.LimitesSalariais.FAIXA2));
		Assert.assertEquals(Constantes.Solidario.Aliquotas.FAIXA3, service.inferirAliquotaContribuicaoPorSalario(Constantes.Solidario.LimitesSalariais.FAIXA2.add(BigDecimal.ONE)));
		Assert.assertEquals(Constantes.Solidario.Aliquotas.FAIXA3, service.inferirAliquotaContribuicaoPorSalario(Constantes.Solidario.LimitesSalariais.FAIXA2.multiply(new BigDecimal(10))));
		
	}
	
	@Test
	public void testeContribuicaoMensal() throws Exception {
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Solidario.LimitesSalariais.FAIXA1).compareTo(Constantes.Solidario.LimitesSalariais.FAIXA1.multiply(Constantes.Solidario.Aliquotas.FAIXA2)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Solidario.LimitesSalariais.FAIXA1).compareTo(Constantes.Solidario.LimitesSalariais.FAIXA1.multiply(Constantes.Solidario.Aliquotas.FAIXA3)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Solidario.LimitesSalariais.FAIXA1.add(BigDecimal.ONE)).compareTo(Constantes.Solidario.LimitesSalariais.FAIXA1.add(BigDecimal.ONE).multiply(Constantes.Solidario.Aliquotas.FAIXA2)) == 0);
		 
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Solidario.LimitesSalariais.FAIXA2).compareTo(Constantes.Solidario.LimitesSalariais.FAIXA2.multiply(Constantes.Solidario.Aliquotas.FAIXA3)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Solidario.LimitesSalariais.FAIXA2.add(BigDecimal.ONE)).compareTo(Constantes.Solidario.LimitesSalariais.FAIXA2.add(BigDecimal.ONE).multiply(Constantes.Solidario.Aliquotas.FAIXA3)) == 0);

		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Solidario.LimitesSalariais.FAIXA2.add(new BigDecimal(10000))).compareTo(Constantes.Solidario.LimitesSalariais.FAIXA2.add(new BigDecimal(10000)).multiply(Constantes.Solidario.Aliquotas.FAIXA3)) == 0);
	}

}
