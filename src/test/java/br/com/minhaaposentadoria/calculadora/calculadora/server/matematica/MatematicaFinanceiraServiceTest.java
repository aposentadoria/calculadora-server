package br.com.minhaaposentadoria.calculadora.calculadora.server.matematica;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MatematicaFinanceiraServiceTest {

	private MatematicaFinanceiraService mtm;

	@Before
	public void setUp() throws Exception {
		mtm = new MatematicaFinanceiraService();
	}

	@Test
	public void testValorFuturo() {
		BigDecimal valorPresente = new BigDecimal(1000);
		BigDecimal taxa = BigDecimal.ONE;
		
		Assert.assertEquals(new BigDecimal(1000), mtm.calcularValorFuturo(valorPresente, taxa, 0));
		Assert.assertEquals(new BigDecimal(2000), mtm.calcularValorFuturo(valorPresente, taxa, 1));
		Assert.assertEquals(new BigDecimal(4000), mtm.calcularValorFuturo(valorPresente, taxa, 2));
		Assert.assertEquals(new BigDecimal(8000), mtm.calcularValorFuturo(valorPresente, taxa, 3));
		Assert.assertEquals(new BigDecimal(16000), mtm.calcularValorFuturo(valorPresente, taxa, 4));
		
		Assert.assertEquals(new BigDecimal(1000), mtm.calcularValorFuturo(valorPresente, BigDecimal.ZERO, 1));
		Assert.assertEquals(new BigDecimal(1000), mtm.calcularValorFuturo(valorPresente, BigDecimal.ZERO, 2));
		Assert.assertEquals(new BigDecimal(1000), mtm.calcularValorFuturo(valorPresente, BigDecimal.ZERO, 3));
		
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularValorFuturo(BigDecimal.ZERO, taxa, 1));
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularValorFuturo(BigDecimal.ZERO, taxa, 2));
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularValorFuturo(BigDecimal.ZERO, taxa, 3));
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularValorFuturo(BigDecimal.ZERO, taxa, 4));
	}
	
	@Test
	public void testInvestimentoAportePeriodicoEntradaZero() throws Exception {
		BigDecimal valorAporte = new BigDecimal(1000);
		BigDecimal taxa = BigDecimal.ONE;
		
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularInvestimentoAporteFixoEntradaZero(valorAporte, taxa, 0));
		Assert.assertEquals(new BigDecimal(2000), mtm.calcularInvestimentoAporteFixoEntradaZero(valorAporte, taxa, 1));
		Assert.assertEquals(new BigDecimal(6000), mtm.calcularInvestimentoAporteFixoEntradaZero(valorAporte, taxa, 2));
		Assert.assertEquals(new BigDecimal(14000), mtm.calcularInvestimentoAporteFixoEntradaZero(valorAporte, taxa, 3));
		
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularInvestimentoAporteFixoEntradaZero(BigDecimal.ZERO, taxa, 1));
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularInvestimentoAporteFixoEntradaZero(BigDecimal.ZERO, taxa, 2));
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularInvestimentoAporteFixoEntradaZero(BigDecimal.ZERO, taxa, 3));
	}
	
	@Test
	public void testInvestimentoAportePeriodico() throws Exception {
		BigDecimal valorAporte = new BigDecimal(1000);
		BigDecimal valorInicial = new BigDecimal(1000);
		BigDecimal taxa = BigDecimal.ONE;
		
		Assert.assertEquals(valorInicial, mtm.calcularInvestimentoAporteFixo(valorInicial, valorAporte, taxa, 0));
		Assert.assertEquals(new BigDecimal(4000), mtm.calcularInvestimentoAporteFixo(valorInicial, valorAporte, taxa, 1));
		Assert.assertEquals(new BigDecimal(10000), mtm.calcularInvestimentoAporteFixo(valorInicial, valorAporte, taxa, 2));
		Assert.assertEquals(new BigDecimal(22000), mtm.calcularInvestimentoAporteFixo(valorInicial, valorAporte, taxa, 3));
		
		Assert.assertEquals(new BigDecimal(2000), mtm.calcularInvestimentoAporteFixo(valorInicial, BigDecimal.ZERO, taxa, 1));
		Assert.assertEquals(new BigDecimal(4000), mtm.calcularInvestimentoAporteFixo(valorInicial, BigDecimal.ZERO, taxa, 2));
		Assert.assertEquals(new BigDecimal(8000), mtm.calcularInvestimentoAporteFixo(valorInicial, BigDecimal.ZERO, taxa, 3));
		
		Assert.assertEquals(valorInicial, mtm.calcularInvestimentoAporteFixo(valorInicial, BigDecimal.ZERO, taxa, 0));
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularInvestimentoAporteFixo(BigDecimal.ZERO, BigDecimal.ZERO, taxa, 1));
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularInvestimentoAporteFixo(BigDecimal.ZERO, BigDecimal.ZERO, taxa, 2));
		Assert.assertEquals(BigDecimal.ZERO, mtm.calcularInvestimentoAporteFixo(BigDecimal.ZERO, BigDecimal.ZERO, taxa, 3));
	}

}
