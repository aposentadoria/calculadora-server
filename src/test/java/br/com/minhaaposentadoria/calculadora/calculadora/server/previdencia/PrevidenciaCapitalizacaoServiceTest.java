package br.com.minhaaposentadoria.calculadora.calculadora.server.previdencia;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.minhaaposentadoria.calculadora.calculadora.server.matematica.MatematicaFinanceiraService;
import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Constantes;
import br.com.minhaaposentadoria.calculadora.calculadora.server.uteis.Genero;

public class PrevidenciaCapitalizacaoServiceTest {
	
	private PrevidenciaCapitalizacaoService service;

	@Before
	public void setUp() throws Exception {
		service = new PrevidenciaCapitalizacaoService(new MatematicaFinanceiraService());
	}
	
	@Test
	public void testeAliquota() throws Exception {
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA1, service.inferirAliquotaContribuicaoPorSalario(BigDecimal.ZERO));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA1, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA1));
		Assert.assertNotEquals(Constantes.Capitalizacao.Aliquotas.FAIXA1, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA2));
		
		Assert.assertNotEquals(Constantes.Capitalizacao.Aliquotas.FAIXA2, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA1));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA2, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA1.add(BigDecimal.ONE)));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA2, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA2));
		
		Assert.assertNotEquals(Constantes.Capitalizacao.Aliquotas.FAIXA3, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA2));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA3, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA2.add(BigDecimal.ONE)));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA3, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA3));
		
		Assert.assertNotEquals(Constantes.Capitalizacao.Aliquotas.FAIXA4, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA3));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA4, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA3.add(BigDecimal.ONE)));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA4, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA4));
		
		Assert.assertNotEquals(Constantes.Capitalizacao.Aliquotas.FAIXA5, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA4));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA5, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA4.add(BigDecimal.ONE)));
		Assert.assertEquals(Constantes.Capitalizacao.Aliquotas.FAIXA5, service.inferirAliquotaContribuicaoPorSalario(Constantes.Capitalizacao.LimitesSalariais.FAIXA4.multiply(new BigDecimal(10))));
	}
	
	@Test
	public void testeContribuicaoMensal() throws Exception {
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA1).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA1.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA2)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA1).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA1.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA3)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA1).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA1.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA4)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA1).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA1.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA5)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA1.add(BigDecimal.ONE)).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA1.add(BigDecimal.ONE).multiply(Constantes.Capitalizacao.Aliquotas.FAIXA2)) == 0);
		 
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA2).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA2.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA3)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA2).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA2.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA4)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA2).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA2.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA5)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA2.add(BigDecimal.ONE)).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA2.add(BigDecimal.ONE).multiply(Constantes.Capitalizacao.Aliquotas.FAIXA3)) == 0);
		 
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA3).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA3.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA4)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA3).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA3.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA5)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA3.add(BigDecimal.ONE)).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA3.add(BigDecimal.ONE).multiply(Constantes.Capitalizacao.Aliquotas.FAIXA4)) == 0);
		 
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA4).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA4.multiply(Constantes.Capitalizacao.Aliquotas.FAIXA5)) < 0);
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA4.add(BigDecimal.ONE)).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA4.add(BigDecimal.ONE).multiply(Constantes.Capitalizacao.Aliquotas.FAIXA5)) == 0);
		 
		 Assert.assertTrue(service.calcularContribuicaoMensal(Constantes.Capitalizacao.LimitesSalariais.FAIXA4.add(new BigDecimal(10000))).compareTo(Constantes.Capitalizacao.LimitesSalariais.FAIXA4.add(new BigDecimal(10000)).multiply(Constantes.Capitalizacao.Aliquotas.FAIXA5)) == 0);
	}
	
	@Test
	public void testeSalarioAposentadoria() throws Exception {
		Assert.assertEquals(new BigDecimal(60).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 20, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(62).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 21, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(64).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 22, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(66).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 23, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(68).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 24, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(70).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 25, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(100).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 40, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(60).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 20, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(62).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 21, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(64).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 22, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(66).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 23, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(68).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 24, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(70).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 25, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(100).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(100), 40, Genero.FEMININO, 62).intValue());

		Assert.assertEquals(new BigDecimal(2000 * 0.6).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 20, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.62).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 21, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.64).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 22, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.66).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 23, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.68).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 24, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.70).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 25, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(2000).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 40, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.60).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 20, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.62).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 21, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.64).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 22, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.66).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 23, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.68).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 24, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(2000 * 0.70).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 25, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(2000).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA2, 40, Genero.FEMININO, 62).intValue());
		
		Assert.assertEquals(new BigDecimal(3000 * 0.6).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 20, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.62).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 21, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.64).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 22, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.66).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 23, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.68).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 24, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.70).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 25, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(3000).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 40, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.60).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 20, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.62).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 21, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.64).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 22, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.66).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 23, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.68).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 24, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(3000 * 0.70).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 25, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(3000).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA3, 40, Genero.FEMININO, 62).intValue());
		
		Assert.assertEquals(new BigDecimal(4500 * 0.6).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 20, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.62).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 21, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.64).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 22, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.66).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 23, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.68).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 24, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.70).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 25, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(4500).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 40, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.60).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 20, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.62).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 21, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.64).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 22, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.66).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 23, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.68).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 24, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(4500 * 0.70).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 25, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(4500).intValue(), service.calcularSalarioAposentadoria(Constantes.Capitalizacao.LimitesSalariais.FAIXA4, 40, Genero.FEMININO, 62).intValue());

		Assert.assertEquals(new BigDecimal(6000).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 20, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(6200).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 21, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(6400).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 22, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(6600).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 23, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(6800).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 24, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(7000).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 25, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(10000).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 40, Genero.MASCULINO, 65).intValue());
		Assert.assertEquals(new BigDecimal(6000).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 20, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(6200).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 21, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(6400).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 22, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(6600).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 23, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(6800).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 24, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(7000).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 25, Genero.FEMININO, 62).intValue());
		Assert.assertEquals(new BigDecimal(10000).intValue(), service.calcularSalarioAposentadoria(new BigDecimal(10000), 40, Genero.FEMININO, 62).intValue());
	}
	
	@Test
	public void testeCalcularMontanteFundoPrevidenciario() throws Exception {
		int periodo = 20;
		BigDecimal salario = new BigDecimal(1000);
		Assert.assertTrue(salario.multiply(new BigDecimal(periodo)).compareTo(service.calcularMontanteContribuicaoPrevidenciaria(salario, periodo, Constantes.TAXA_CDI_04_2019_ANUAL)) < 0);
	}

}
