FROM openjdk:13-alpine
WORKDIR /app
EXPOSE 8080
COPY ./target/calculadora-server*.jar /app/calculadora-server.jar
CMD ["java","-jar","calculadora-server.jar"]
